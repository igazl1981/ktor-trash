import axios from "axios";

export default {
    getAll() {
        return axios.get('/todo/items')
            .then(response => {
                return response.data
            })
    },
    selectTodos(selectedIds, unselectedIds) {
        let todoIds = [];
        selectedIds.forEach(item => todoIds.push({_id: item, finished: true}))
        unselectedIds.forEach(item => todoIds.push({_id: item, finished: false}))
        return axios.post("/todo/selected-items", {todos: todoIds})
            .then(response => {
                return response.data
            })
    },
    add(message) {
        return axios.post('/todo/items', {message: message})
            .then(response => {
                return response.data
            })
    }
}