package trash

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.jackson.jackson
import io.ktor.routing.Routing
import io.ktor.server.engine.commandLineEnvironment
import io.ktor.server.engine.embeddedServer
import io.ktor.server.jetty.Jetty
import org.bson.UuidRepresentation
import org.koin.dsl.module
import org.koin.ktor.ext.Koin
import org.litote.kmongo.KMongo
import org.litote.kmongo.id.jackson.IdJacksonModule
import trash.routes.todoRoutes

fun main(args: Array<String>) {
    embeddedServer(Jetty, commandLineEnvironment(args)).start(true)
}

//class ApplicationData {
//    companion object {
//        val todos = mutableListOf(
//            Todo(UUID.randomUUID(), "My first Todo", false),
//            Todo(UUID.randomUUID(), "My second Todo", true),
//            Todo(UUID.randomUUID(), "My third Todo", false),
//            Todo(UUID.randomUUID(), "My fourth Todo", false)
//        )
//    }
//}

fun Application.main() {
    install(DefaultHeaders) {
        header("X-Whatever", "This is a whatever default header")
        header(HttpHeaders.Server, "The server is mysterious")
    }
    install(CallLogging)
    install(ContentNegotiation) {
        jackson {
            registerModule(IdJacksonModule())
        }
    }
    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.AccessControlAllowHeaders)
        header(HttpHeaders.ContentType)
        header(HttpHeaders.AccessControlAllowOrigin)
        anyHost()
    }
    install(Koin) {
        modules(module)
    }
    install(Routing) {
        todoRoutes()
    }
}

val module = module {
    single {
        val settings = MongoClientSettings.builder()
            .uuidRepresentation(UuidRepresentation.STANDARD)
            .applyConnectionString(ConnectionString("mongodb://todo-trash:welkom123@trash-cluster-shard-00-00.fd99u.mongodb.net:27017,trash-cluster-shard-00-01.fd99u.mongodb.net:27017,trash-cluster-shard-00-02.fd99u.mongodb.net:27017/trash?ssl=true&replicaSet=atlas-w0bhub-shard-0&authSource=admin&retryWrites=true&w=majority"))
            .build()

        KMongo.createClient(settings)
    }
}