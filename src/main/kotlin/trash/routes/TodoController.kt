package trash.routes

import com.mongodb.client.MongoClient
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId
import org.koin.ktor.ext.inject
import org.litote.kmongo.*
import org.litote.kmongo.id.toId
import org.slf4j.Logger
import org.slf4j.LoggerFactory


const val collectionName = "todos"

fun Route.todoRoutes() {
    val log: Logger = LoggerFactory.getLogger("UserController")
    val client: MongoClient by inject()

    get("/") {
        log.info("##############################")
        call.respondText("The first Ktor Trash endpoint", ContentType.Text.Plain)
    }

    get("/todo/items") {
        log.info("Retrieve todo items")
        val todos = getTodos(client)

        call.respond(todos)
    }

    post("/todo/items") {
        log.info("Create todo item")
        val newTodo = call.receive<Todo>()
        val collection = client.getDatabase("trash")
            .getCollection<Todo>(collectionName)

        collection.insertOne(newTodo)

        val instertedTodo =
            collection.find().sort("{_id: -1}").limit(1).firstOrNull() ?: throw RuntimeException("Cannot insert")

        call.respond(instertedTodo)
    }

    post("/todo/selected-items") {
        log.info("Selecting items todo items")
        val selectedItemsRequests = call.receive<TodoSelectRequest>()

        val collection = client.getDatabase("trash")
            .getCollection<Todo>(collectionName)

        val bulkWrite = selectedItemsRequests.todos.map {
            updateOne<Todo>(filter = Todo::_id eq it._id, update = setValue(Todo::finished, it.finished))
        }.toMutableList()

        val bulkWriteResult = collection.bulkWrite(bulkWrite)
        log.info("##############3 $bulkWriteResult")
        call.respond(getTodos(client))
    }

    get("/gimmeJson") {
        call.respond(SampleResponse("This should be json"))
    }

    post("/processJson") {
        val data = call.receive<SampleRequest>()
        log.info(data.requestMessage)
        call.respondText("Post Happened", ContentType.Text.Plain)
    }

}

private fun getTodos(
    client: MongoClient
): List<Todo> {
    val collection = client.getDatabase("trash")
        .getCollection<Todo>(collectionName)

    return collection.find().sort(ascending(Todo::finished)).toList()
}

data class TodoSelectRequest(val todos: List<Todo>)
data class TodoSelect(val id: String, val finished: Boolean)
data class Todo(@BsonId val _id: Id<Todo>?, val message: String? = null, val finished: Boolean = false)
data class SampleResponse(val message: String)
data class SampleRequest(val requestMessage: String)